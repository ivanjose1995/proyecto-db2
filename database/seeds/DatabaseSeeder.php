<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ClientesTableSeeder::class);
         $this->call(SucursalsTableSeeder::class);
         $this->call(IngredientesTableSeeder::class);
         $this->call(RecetasTableSeeder::class);
         $this->call(ProductosTableSeeder::class);
         $this->call(VentasTableSeeder::class);
    }
}
