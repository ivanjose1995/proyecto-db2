<?php

use Illuminate\Database\Seeder;

use App\Receta;
use App\Ingrediente;

class RecetasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$ingredientes = Ingrediente::all();

        $receta = Receta::create([
            'salchicha' => 'salchicha polaca',
		]);
		$ingredientes->each(function($i) use ($receta){
			$receta->ingredientes()->attach($i);
		});

        $receta = Receta::create([
            'salchicha' => 'salchicha Frankfurt',
		]);
		$ingredientes->each(function($i) use ($receta){
			$receta->ingredientes()->attach($i);
		});

        $receta = Receta::create([
            'salchicha' => 'salchicha Kosaka',
		]);
		$ingredientes->each(function($i) use ($receta){
			$receta->ingredientes()->attach($i);
		});

        $receta = Receta::create([
            'salchicha' => 'salchicha 4 quesos',
		]);
		$ingredientes->each(function($i) use ($receta){
			$receta->ingredientes()->attach($i);
		});
    }
}
