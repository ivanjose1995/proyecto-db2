<?php

use Illuminate\Database\Seeder;

class SucursalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sucursals')->insert([
            'nombre' => 'Sucursal Barrio Obrero',
            'ubicacion' => 'Barrio Obrero,Calle 11, Carrera 19',
        ]);
        DB::table('sucursals')->insert([
            'nombre' => 'Sucursal Pueblo Nuevo',
            'ubicacion' => 'Av principal Pueblo nuevo,CC Monte Carlo',
        ]);
        DB::table('sucursals')->insert([
            'nombre' => 'Sucursal Centro',
            'ubicacion' => '5ta Av,entre calles 15 y 16',
        ]);
    }
}
