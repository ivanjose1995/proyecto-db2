<?php

use Illuminate\Database\Seeder;

class IngredientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredientes')->insert([
            'ingrediente' => 'papas',
		]);
		DB::table('ingredientes')->insert([
            'ingrediente' => 'ensalada',
		]);
		DB::table('ingredientes')->insert([
            'ingrediente' => 'cebolla',
		]);
		DB::table('ingredientes')->insert([
            'ingrediente' => 'queso',
		]);
    }
}
