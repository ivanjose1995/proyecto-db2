<?php

use App\Cliente;
use App\Precio;
use App\Producto;
use App\Venta;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VentasTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$dias = 0;
		$clientes = 0;
		// numero de ventas antes de aumentar al siguiente dia
		$numeroVentasDia = rand(5, 30);
		// numero de dias antes de aumentar los precios
		$numeroDiasAumentoPrecio = rand(3, 9);
		for ($i = 0; $i < 500; ++$i) {
			$fecha = Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-01 08:00:00');
			$horas = rand(0, 8);
			$minutos = rand(0, 60);

			$fecha->addDays($dias);

			// Calculos al inicio del dia siguiente
			if ($numeroVentasDia == $i) {
				++$dias;
				$numeroVentasDia = $i + rand(5, 30);
				if ($dias == $numeroDiasAumentoPrecio) {
					// Aumentar precios cada cierto tiempo
					Producto::all()->each(function ($p) use ($fecha) {
						$precioActual = Precio::where('producto_id', $p->id)
							->orderBy('created_at', 'desc')
							->first()->precio;

						$precioNuevo = $precioActual * (1 + (rand(1, 3) / 10));
						$precioNuevo = round($precioNuevo / 100) * 100;
						$precioNuevo = $precioNuevo > $precioActual ? $precioNuevo : $precioActual;
						Precio::create(['producto_id' => $p->id, 'precio' => $precioNuevo, 'created_at' => $fecha]);
					});
					$numeroDiasAumentoPrecio = $dias + rand(3, 9);
				}
			}

			$fecha->addHours($horas);
			$fecha->addMinutes($minutos);

			$esClienteNuevo = (150 - rand(0, $clientes)) > 100;

			if ($esClienteNuevo) {
				$cliente = factory(App\Cliente::class)->create(['created_at' => $fecha, 'updated_at' => $fecha]);
				++$clientes;
			} else {
				$cliente = Cliente::inRandomOrder()->first();
			}
			$venta = factory(App\Venta::class)->create(['created_at' => $fecha, 'updated_at' => $fecha, 'cliente_id' => $cliente->id]);

			// Si se alcanza el numero de ventas del dia se aumenta y se genera otro numero

			// Agregamos de 1 a 5 productos a la venta
			for ($j = 0; $j < rand(1, 5); ++$j) {
				$producto = Producto::inRandomOrder()->first();
				$ingredientes_removidos = 0;
				if ($producto->receta_id) {
					$ingredientesTotal = $producto->receta->ingredientes->count();
					$ingredientes_removidos = rand(0, 10) > 5 ? rand(1, $ingredientesTotal) : 0;
				}
				$venta->productos()->attach($producto, ['ingredientes_removidos' => $ingredientes_removidos]);
			}
		}
	}
}
