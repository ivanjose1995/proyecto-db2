<?php

use App\Precio;
use App\Producto;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Producto::reguard();
		Precio::reguard();
		$productos = [
			[
				'receta_id' => '1',
				'nombre' => 'Perro caliente Polaco',
				'precio' => '1200',
				'porcentaje_ganancia' => 30,
			],
			[
				'receta_id' => '2',
				'nombre' => 'Perro caliente Frankfurt',
				'precio' => '1000',
				'porcentaje_ganancia' => 30,
			],
			[
				'receta_id' => '3',
				'nombre' => 'Perro caliente Kosako',
				'precio' => '1000',
				'porcentaje_ganancia' => 30,
			],
			[
				'receta_id' => '4',
				'nombre' => 'Perro caliente 4 quesos',
				'precio' => '1100',
				'porcentaje_ganancia' => 30,
			],
			[
				'receta_id' => null,
				'nombre' => 'refresco 300ml',
				'precio' => '500',
				'porcentaje_ganancia' => 5,
			],
		];

		foreach ($productos as $key => $p) {
			$producto = new Producto();
			$producto->fill($p);
			$producto->save();

			$fecha = Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-01 08:00:00');

			$precio = new Precio();
			$precio->fill([
				'producto_id' => $producto->id, 'precio' => $p['precio'],
			]);
			$precio->created_at = $fecha;
			$precio->save();
		}
	}
}
