<?php

// @var $factory \Illuminate\Database\Eloquent\Factory

use App\Sucursal;
use App\Venta;
use Faker\Generator as Faker;

$factory->define(Venta::class, function (Faker $faker) {
	return [
		'sucursal_id' => Sucursal::inRandomOrder()->first()->id,
	];
});
