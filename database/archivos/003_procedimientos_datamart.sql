-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.actualizar_datamart
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_datamart`()
    MODIFIES SQL DATA
BEGIN

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE table fact_producto_venta;
TRUNCATE table dim_fecha;
TRUNCATE table dim_producto;
TRUNCATE table dim_venta;
TRUNCATE table dim_cliente;

SET FOREIGN_KEY_CHECKS = 1;

CALL `actualizar_dim_fecha`();
CALL `actualizar_dim_cliente`();
CALL `actualizar_dim_venta`();
CALL `actualizar_dim_producto`();
CALL `actualizar_fact_producto_venta`();

END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.actualizar_dim_cliente
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_dim_cliente`()
    MODIFIES SQL DATA
BEGIN
	
	FOR row IN 	(
					SELECT c.id,c.nombre,c.created_at, (
																	SELECT MAX(v.created_at) 
																	FROM proyecto_db2_perros_calientes.ventas v
																	WHERE (v.cliente_id = c.id)
																	) as "ultima_visita"
					FROM proyecto_db2_perros_calientes.clientes c
					)
	DO
		INSERT INTO dim_cliente VALUES (row.id,row.nombre,row.created_at,row.ultima_visita);
	END FOR;

END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.actualizar_dim_fecha
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_dim_fecha`()
    MODIFIES SQL DATA
BEGIN

	DECLARE dia DATE;
	DECLARE ultimo_dia DATE;
	
	SET lc_time_names = 'es_ES';
	
	SELECT 
	MIN(CAST(v.created_at AS DATE)), MAX(CAST(v.created_at AS DATE)) 
	INTO dia,ultimo_dia
	FROM proyecto_db2_perros_calientes.ventas as v;
	
	
  	WHILE ultimo_dia >= dia DO
		INSERT INTO dim_fecha (fecha,turno,dia) VALUES (dia,'mañana',DAYNAME(dia));
		INSERT INTO dim_fecha (fecha,turno,dia) VALUES (dia,'tarde',DAYNAME(dia));
    	SET dia = ADDDATE(dia,1);
  	END WHILE;
	

END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.actualizar_dim_producto
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_dim_producto`()
    MODIFIES SQL DATA
BEGIN

	
	FOR row IN (
					SELECT p.id,p.nombre,p.porcentaje_ganancia,r.salchicha,pf.precio as "precio_actual",
							 ISNULL(p.receta_id) as "es_refresco", NOT ISNULL(p.receta_id) as "es_perro",
							 COUNT(ing.id) as "ingredientes"
					FROM proyecto_db2_perros_calientes.productos p
					LEFT JOIN proyecto_db2_perros_calientes.recetas r ON (p.receta_id = r.id)
					JOIN proyecto_db2_perros_calientes.precios_fecha_fin pf ON (p.id = pf.producto_id AND ISNULL(pf.changed_at))
					LEFT JOIN proyecto_db2_perros_calientes.ingrediente_receta ing_rec ON (ing_rec.receta_id = r.id)
					LEFT JOIN proyecto_db2_perros_calientes.ingredientes ing ON (ing_rec.ingrediente_id = ing.id)
					GROUP BY (p.id)
	)
	DO
		-- SELECT row.id,row.nombre,row.es_perro, row.es_perro;
		INSERT INTO dim_producto (id,nombre,precio_actual,es_perro,es_refresco,salchicha,ingredientes,porcentaje_ganancia)
		VALUES (row.id,row.nombre,row.precio_actual,row.es_perro,row.es_refresco,row.salchicha,row.ingredientes,row.porcentaje_ganancia);
	END FOR;
END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.actualizar_dim_venta
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_dim_venta`()
    MODIFIES SQL DATA
BEGIN
	
	FOR row IN (
					SELECT v.id,v.created_at, SUM(precio.precio) as "precio_total", SUM(precio.precio * p.porcentaje_ganancia) as "ganancia_total",
					s.nombre as "sucursal_nombre", s.ubicacion as "sucursal_ubicacion"
					FROM proyecto_db2_perros_calientes.producto_venta pv
					JOIN proyecto_db2_perros_calientes.ventas v 
					ON (v.id = pv.venta_id)
					JOIN proyecto_db2_perros_calientes.sucursals s
					ON (v.sucursal_id = s.id)
					JOIN proyecto_db2_perros_calientes.productos p
					ON (pv.producto_id = p.id)
					JOIN proyecto_db2_perros_calientes.precios_fecha_fin precio
					ON (precio.producto_id = p.id AND v.created_at >= precio.created_at AND v.created_at < IFNULL(precio.changed_at,'2099-01-01'))
					GROUP BY v.id
					)
	DO
		INSERT INTO dim_venta VALUES (row.id,row.precio_total,row.ganancia_total,row.sucursal_nombre,row.sucursal_ubicacion);
	END FOR;
END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.actualizar_fact_producto_venta
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_fact_producto_venta`()
    MODIFIES SQL DATA
BEGIN
	
	
	FOR row IN (
			SELECT vp.*,v.cliente_id as "cliente_id", pr.precio as "precio",
					 CAST(v.created_at as DATE) as fecha,
					 if(CAST(v.created_at as TIME) > '12:00','tarde','mañana') as turno,
					 dim_f.id as "fecha_id"
			FROM proyecto_db2_perros_calientes.producto_venta as vp
			JOIN proyecto_db2_perros_calientes.ventas as v ON (v.id = vp.venta_id)
			JOIN proyecto_db2_perros_calientes.precios_fecha_fin pr
			ON (
					pr.producto_id = vp.producto_id AND
					v.created_at >= pr.created_at AND
					v.created_at < IFNULL(pr.changed_at,'2099-01-01')
				)
			JOIN dim_fecha as dim_f ON (CAST(v.created_at as DATE) = dim_f.fecha AND IF(CAST(v.created_at as TIME) > '12:00','tarde','mañana') = dim_f.turno)
	)
	DO
		INSERT INTO fact_producto_venta (producto_id,venta_id,cliente_id,fecha_id,ingredientes_removidos,precio) 
		VALUES (row.producto_id,row.venta_id,row.cliente_id,row.fecha_id,row.ingredientes_removidos,row.precio);
	END FOR;
END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.analisis_cohorte
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `analisis_cohorte`(
	IN `fecha` DATETIME








)
BEGIN
/*
	Recibe una fecha para indicar la semana y 
	retorna el total de clientes, clientes nuevos,
	clientes viejos y clientes regulares de esa semana.
*/

DECLARE semana_inicio INTEGER;
DECLARE semana_actual INTEGER;

DECLARE clientes_semana INTEGER;
DECLARE clientes_viejos INTEGER;
DECLARE clientes_regulares INTEGER DEFAULT 0;

DECLARE primera_visita INTEGER;
DECLARE semanas_visitas INTEGER;

SELECT WEEK(MIN(d_f.fecha),1) INTO semana_inicio FROM dim_fecha d_f;
SET semana_actual = WEEK(fecha,1);


SELECT
	(
		SELECT COUNT(DISTINCT pv.cliente_id)
		FROM fact_producto_venta pv
		JOIN dim_fecha f
		ON (pv.fecha_id = f.id AND WEEK(f.fecha,1) = semana_actual)
	),
	(
		SELECT COUNT(DISTINCT pv_a.cliente_id) as "CLIENTES VIEJOS"
		FROM fact_producto_venta pv_a
		JOIN dim_fecha f_a
		ON (pv_a.fecha_id = f_a.id AND WEEK(f_a.fecha,1) = semana_actual)
		INNER JOIN 	(
							SELECT pv_b.cliente_id
							FROM fact_producto_venta pv_b
							JOIN dim_fecha f_b
							ON (pv_b.fecha_id = f_b.id AND WEEK(f_b.fecha,1) < semana_actual)
						) visita_anterior
		ON (visita_anterior.cliente_id = pv_a.cliente_id )
	) INTO clientes_semana,clientes_viejos;



FOR row IN 	(
							SELECT pv_a.cliente_id
							FROM fact_producto_venta pv_a
							JOIN dim_fecha f_a
							ON (pv_a.fecha_id = f_a.id AND WEEK(f_a.fecha,1) = semana_actual)
							INNER JOIN 	(
												SELECT pv_b.cliente_id
												FROM fact_producto_venta pv_b
												JOIN dim_fecha f_b
												ON (pv_b.fecha_id = f_b.id AND WEEK(f_b.fecha,1) < semana_actual)
											) visita_anterior
							ON (visita_anterior.cliente_id = pv_a.cliente_id )
							GROUP BY pv_a.cliente_id
						)
DO
	
	SELECT MIN(WEEK(f.fecha,1)),COUNT(DISTINCT WEEK(f.fecha,1)) INTO primera_visita, semanas_visitas
	FROM fact_producto_venta pv
	JOIN dim_fecha f
	ON (pv.fecha_id = f.id AND pv.cliente_id = row.cliente_id AND  WEEK(f.fecha,1) <= semana_actual);
	IF semanas_visitas >= (semana_actual - primera_visita) * 0.8
	THEN 	/* 
				Si ha venido a mas o igual al 80% de las semanas que 
				han pasado desde su primera visita hasta la semana especificada, se considera regular.
			*/
		SET clientes_regulares = clientes_regulares + 1;
	END IF;
END FOR;
SELECT 	clientes_semana as "TOTAL SEMANA",
			clientes_semana - clientes_viejos as "CLIENTES NUEVOS",
			clientes_viejos AS "CLIENTES VIEJOS",
			clientes_regulares AS "CLIENTES REGULARES";

END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.ganancias_semanales
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `ganancias_semanales`()
    READS SQL DATA
BEGIN

SET @COLUMNAS = '';
SET SESSION group_concat_max_len = 100000;

FOR row in (
		SELECT * FROM dim_venta GROUP BY sucursal
) DO
	SET @COLUMNAS = CONCAT(@COLUMNAS,'SUM( IF ( v.sucursal = \'',row.sucursal,'\',v.ganancia_total,0','))',' as "',row.sucursal,'",');
END FOR;

SET @COLUMNAS = SUBSTRING(@COLUMNAS,1,LENGTH(@COLUMNAS)-1);

SET @STATEMENT = CONCAT(
--		'SELECT f.fecha, CONCAT(f.dia,\' en la \',f.turno) AS "DIA", sum(v.total) as "TOTAL_GANANCIA", ',
		'SELECT f.dia, sum(v.ganancia_total) as "TOTAL_GANANCIA", ',
		@COLUMNAS,
		'
			FROM dim_fecha f
			JOIN fact_producto_venta pv ON (pv.fecha_id = f.id)
			JOIN dim_venta v ON (pv.venta_id = v.id)
			GROUP BY f.dia
		'
);

-- SELECT @STATEMENT;

PREPARE stmt FROM @STATEMENT;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


    
END//
DELIMITER ;

-- Dumping structure for procedure proyecto_db2_perros_calientes_datamart.ventas_semanales
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_semanales`()
    READS SQL DATA
BEGIN

SET @COLUMNAS = '';
SET SESSION group_concat_max_len = 100000;

FOR row in (
		SELECT p.nombre FROM dim_producto p WHERE p.es_perro = 1
) DO
	SET @COLUMNAS = CONCAT(@COLUMNAS,'COUNT( IF ( p.nombre = \'',row.nombre,'\',1,NULL','))',' as "',row.nombre,'",');
END FOR;

SET @COLUMNAS = SUBSTRING(@COLUMNAS,1,LENGTH(@COLUMNAS)-1);

SET @STATEMENT = CONCAT(
		'SELECT f.fecha, CONCAT(f.dia,\' en la \',f.turno) AS "DIA", COUNT(p.nombre) as "TOTAL", ',
		@COLUMNAS,
		'
			FROM dim_fecha f
			JOIN fact_producto_venta pv ON (pv.fecha_id = f.id)
			JOIN dim_producto p ON (pv.producto_id = p.id)
			WHERE p.es_perro = 1
			GROUP BY f.dia,f.turno
		'
);

-- SELECT @STATEMENT;

PREPARE stmt FROM @STATEMENT;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


    
END//
DELIMITER ;

-- Dumping structure for event proyecto_db2_perros_calientes_datamart.carga_semanal
DELIMITER //
CREATE DEFINER=`root`@`%` EVENT `carga_semanal` ON SCHEDULE EVERY 1 WEEK STARTS '2019-08-09 16:39:38' ON COMPLETION PRESERVE ENABLE DO BEGIN
	CALL actualizar_datamart();
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
