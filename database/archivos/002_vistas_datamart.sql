-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view proyecto_db2_perros_calientes_datamart.clientes_dia
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `clientes_dia` (
	`fecha` DATE NOT NULL,
	`clientes_atendidos` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view proyecto_db2_perros_calientes_datamart.clientes_refrescos
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `clientes_refrescos` (
	`Porcentaje de pedidos con refrescos` DECIMAL(27,4) NULL
) ENGINE=MyISAM;

-- Dumping structure for view proyecto_db2_perros_calientes_datamart.ventas_semanales
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `ventas_semanales` (
	`fecha` DATE NOT NULL,
	`dia` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`total_perros_vendidos` BIGINT(21) NOT NULL,
	`Perro caliente 4 quesos` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view proyecto_db2_perros_calientes_datamart.clientes_dia
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `clientes_dia`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clientes_dia` AS select `f`.`fecha` AS `fecha`,count(distinct `pv`.`cliente_id`) AS `clientes_atendidos` from (`fact_producto_venta` `pv` join `dim_fecha` `f` on(`pv`.`fecha_id` = `f`.`id`)) group by `f`.`fecha`;

-- Dumping structure for view proyecto_db2_perros_calientes_datamart.clientes_refrescos
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `clientes_refrescos`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clientes_refrescos` AS select count(0) / (select count(0) from `proyecto_db2_perros_calientes_datamart`.`dim_venta`) * 100 AS `Porcentaje de pedidos con refrescos` from (select count(if(`p`.`es_refresco`,1,NULL)) AS `refrescos_comprados` from ((`proyecto_db2_perros_calientes_datamart`.`fact_producto_venta` `pv` join `proyecto_db2_perros_calientes_datamart`.`dim_venta` `v` on(`pv`.`venta_id` = `v`.`id`)) join `proyecto_db2_perros_calientes_datamart`.`dim_producto` `p` on(`pv`.`producto_id` = `p`.`id`)) group by `v`.`id` having `refrescos_comprados` > 0 and `refrescos_comprados` <> count(0)) `pedidos_con_refrescos`;

-- Dumping structure for view proyecto_db2_perros_calientes_datamart.ventas_semanales
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `ventas_semanales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ventas_semanales` AS select `f`.`fecha` AS `fecha`,`f`.`dia` AS `dia`,count(0) AS `total_perros_vendidos`,count(if(`p`.`nombre` = 'Perro caliente 4 quesos',1,NULL)) AS `Perro caliente 4 quesos` from ((`dim_fecha` `f` join `fact_producto_venta` `pv` on(`pv`.`fecha_id` = `f`.`id`)) join `dim_producto` `p` on(`pv`.`producto_id` = `p`.`id`)) where `p`.`es_perro` = 1 group by `f`.`id`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
