-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view proyecto_db2_perros_calientes.precios_fecha_fin
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `precios_fecha_fin` (
	`precio` INT(11) NOT NULL,
	`producto_id` BIGINT(20) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NULL,
	`changed_at` TIMESTAMP NULL
) ENGINE=MyISAM;

-- Dumping structure for view proyecto_db2_perros_calientes.precios_fecha_fin
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `precios_fecha_fin`;
CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `precios_fecha_fin` AS select `p`.`precio` AS `precio`,`p`.`producto_id` AS `producto_id`,`p`.`created_at` AS `created_at`,(select min(`p2`.`created_at`) from `precios` `p2` where `p`.`producto_id` = `p2`.`producto_id` and `p2`.`created_at` > `p`.`created_at`) AS `changed_at` from `precios` `p`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
