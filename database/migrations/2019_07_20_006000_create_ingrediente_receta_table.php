<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredienteRecetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingrediente_receta', function (Blueprint $table) {
			$table->bigIncrements('id');
			
			$table->unsignedBigInteger('ingrediente_id');	
			$table->foreign('ingrediente_id')->references('id')->on('ingredientes');
			$table->unsignedBigInteger('receta_id');	
            $table->foreign('receta_id')->references('id')->on('recetas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingrediente_receta');
    }
}
