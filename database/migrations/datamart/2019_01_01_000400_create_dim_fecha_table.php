<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimFechaTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('dim_fecha', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->date('fecha');
			$table->string('turno');
			$table->string('dia');

			$table->unique(['fecha', 'turno']);
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('dim_fecha');
	}
}
