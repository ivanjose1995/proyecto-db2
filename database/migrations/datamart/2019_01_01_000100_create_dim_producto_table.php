<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimProductoTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('dim_producto', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('nombre');
			$table->decimal('porcentaje_ganancia', 6, 3);
			$table->integer('precio_actual');
			$table->boolean('es_perro')->default(false);
			$table->boolean('es_refresco')->default(false);
			$table->string('salchicha')->nullable();
			$table->integer('ingredientes')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('dim_producto');
	}
}
