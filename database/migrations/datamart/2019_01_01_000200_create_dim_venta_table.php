<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimVentaTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('dim_venta', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('precio_total');
			$table->integer('ganancia_total');
			$table->string('sucursal');
			$table->string('ubicacion');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('dim_venta');
	}
}
