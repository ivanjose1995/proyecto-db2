<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimClienteTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('dim_cliente', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('nombre');
			$table->dateTime('primera_visita');
			$table->dateTime('ultima_visita');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('dim_cliente');
	}
}
