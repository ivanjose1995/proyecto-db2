<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactProductoVentaTable extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up()
	{
		Schema::create('fact_producto_venta', function (Blueprint $table) {
			$table->bigIncrements('id');

			$table->unsignedBigInteger('fecha_id');
			$table->foreign('fecha_id')->references('id')->on('dim_fecha');

			$table->unsignedBigInteger('cliente_id');
			$table->foreign('cliente_id')->references('id')->on('dim_cliente');

			$table->unsignedBigInteger('venta_id');
			$table->foreign('venta_id')->references('id')->on('dim_venta');

			$table->unsignedBigInteger('producto_id');
			$table->foreign('producto_id')->references('id')->on('dim_producto');

			$table->integer('precio');
			$table->integer('ingredientes_removidos')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down()
	{
		Schema::dropIfExists('fact_producto_venta');
	}
}
