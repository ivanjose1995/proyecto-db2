<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
	//
	public $timestamps = false;
	protected $fillable = ['ingrediente'];
}
