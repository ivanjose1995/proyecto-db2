<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    public function ingredientes(){
		return $this->belongsToMany('App\Ingrediente','ingrediente_receta','receta_id','ingrediente_id');
	}
}
