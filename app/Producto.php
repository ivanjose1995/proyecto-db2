<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
	protected $fillable = ['receta_id', 'nombre', 'porcentaje_ganancia'];

	public function receta()
	{
		return $this->belongsTo('App\Receta');
	}
}
