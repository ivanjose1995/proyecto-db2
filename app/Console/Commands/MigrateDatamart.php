<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateDatamart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:datamart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info('Corriendo migrate:fresh para Datamart');
		$database = 'datamart';
    	$this->call('migrate:fresh', array('--database' => $database, '--path' => 'database/migrations/datamart/'));
    }
}
