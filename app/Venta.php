<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
	//
	

	public function productos(){
		return $this->belongsToMany('App\Venta','producto_venta','venta_id','producto_id')
		->withPivot('ingredientes_removidos');
	}
}
